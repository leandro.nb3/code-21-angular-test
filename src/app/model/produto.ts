export class Produto {
  _id: string;
  name: string;
  description: string;
  price: number;
  pictureUrl: string;
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProdutoDetalheComponent } from './produto-detalhe/produto-detalhe.component';
import { ProdutosComponent } from './produtos/produtos.component';


const routes: Routes = [
  {
    path: 'produtos',
    component: ProdutosComponent,
    data: {title: 'Lista de Produtos'}
  },
  {
    path: 'produto-detalhe/:id',
    component: ProdutoDetalheComponent,
    data: {title: 'Detahes do Produto'}
  },
  {
    path: '',
    redirectTo: '/produtos',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

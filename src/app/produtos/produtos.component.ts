import { Component, OnInit } from '@angular/core';
import { ApiService } from './../api.service';
import { Produto } from '../model/produto';
import { Router } from '@angular/router';


@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.scss']
})
export class ProdutosComponent implements OnInit {

  displayedColumns: string[] = [ 'foto', 'nome', 'desc', 'preco', 'acao'];
  items: Produto[];
  isLoadingResults = true;

  constructor(private _api: ApiService, private router: Router) {

   }

  ngOnInit() {
    this._api.getProdutos()
    .subscribe(res => {
      this.items = res['items'];
      console.log(this.items);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }

  goToProduct(id){
    console.log("id", id)
    this.router.navigate([`/produto-detalhe/${id}`])
  }

}
